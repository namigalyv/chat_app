using System.Collections.Generic;
using System.Linq;
using chat_app.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace chat_app.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ValuesController: Controller
    {
        private ChatDbContext context;

        public ValuesController(ChatDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return context.Users.Select(u => u.UserName).ToArray();
        }
    }
}