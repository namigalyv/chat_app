"use strict";

var connection = new signalR.HubConnectionBuilder()
                        .withUrl("/messages")
                        .build();

var random = Math.floor(Math.random() * 300) + 1;
var user;
function loadPage(){
    user = prompt("Please enter your name");
    if(user == ''){
        user = "guest"+random;
    }
}


connection.on("ReceiveMessage", function(username ,message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    
    var div = document.createElement("div");
    div.innerHTML ="<b>" + username + "</b>: " + msg + "<hr/>";
    
    document.getElementById("messages").appendChild(div);
});

connection.on("UserConnected", function(connectionId){
    var groupElement = document.getElementById("group");
    var option = document.createElement("option");
    option.text = connectionId;
    option.value = connectionId;
    groupElement.add(option);
    // var response = []

    // const Http = new XMLHttpRequest();
    // const url='https://localhost:5001/announcement';
    // Http.open("GET", url);
    // Http.send();
    // var el = document.createElement( 'html' );

    // Http.onreadystatechange=(e)=>{
    //     el.innerHTML = Http.responseText
    // }

    // const formsList = el.getElementsByTagName('li');
    // console.log(formsList);
    // for (var i=0; i < formsList.length; i++) {
    //     var form = formsList[i];
    //     for (field in form) {
    //         var value = form[field].value;
    //         console.log(value);
    //     }
    //}

});

connection.on("UserDisconnected", function(connectionId) {
    var groupElement = document.getElementById("group");
    for(var i = 0; i< groupElement.length; i++){
        if(groupElement.options[i].value == connectionId){
            groupElement.remove(i);
        }
    }
});

connection.start().catch(function(err) {
    return console.error(err.toString());
});


document.getElementById("sendButton").addEventListener("click", function(event) {
    var message = document.getElementById("message").value;
    var groupElement = document.getElementById("group");
    var groupValue = groupElement.options[groupElement.selectedIndex].value;
    
    if(groupValue === "All" || groupValue === "Myself") {
        var method = groupValue === "All" ? "SendMessageToAll" : "SendMessageToCaller";
        connection.invoke(method, user, message).catch(function (err) {
            return console.error(err.toString());
        });
    }else if(groupValue === "PrivateGroup") {
        console.log("group group");
        connection.invoke("SendMessageToGroup", "PrivateGroup", message).catch(function (err) {
            return console.error(err.toString());
        });
    }else{
        connection.invoke("SendMessageToUser", groupValue, message).catch(function (err) {
            return console.error(err.toString());
        });
    }
    
    event.preventDefault();
});

document.getElementById("joinGroup").addEventListener("click", function(event) {
    console.log("join join");
    connection.invoke("JoinGroup", "PrivateGroup").catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});
